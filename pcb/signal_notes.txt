17 pins	RAS_n,A15:0 - address
1 pin	ACT_n - command vs data
2 pins	BA1:0 - more "address" bits
2 pins	BG1:0 - more "address" bits
2 pins	CK_t,CK_c - clock differential
1 pin	CKE - clock enable
1 pin	CS_n - chip select
1 pins	DM_n - data valid
1 pin	ODT - ? (maybe not needed)
1 pin	RESET_n - reset
8 pins	DQ[7:0] - data
2 pins	DQS_t,DQS_c - data strobe differential
2 pins	TQDS_t,TQDS_c - ? (maybe not needed)
2 pins	VDD,VDDQ - 1.2V
1 pin	VPP - 2.5V
1 pin	VREFCA - .6V
2 pins	VSS,VSSQ - GND
1 pin	ZQ - ? (maybe not needed)

unused/unrouted:
PAR
TEN
ALERT
